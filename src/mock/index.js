import Mock from 'mockjs'
import phpMenus from './data/php-menus'
import userLogin from './data/login'
import monthRankingData2 from './data/minzheng/month-ranking-data2'
import monthRankingSq from './data/minzheng/month-ranking-sq'
// 配置Ajax请求延时，可用来测试网络延迟大时项目中一些效果
Mock.setup({
  timeout: 1000
})

// 登录相关和获取用户信息
Mock.mock(/\/login_NoUse/, userLogin)
Mock.mock(/\/wl\/menus/, phpMenus)
Mock.mock(/\/mock\/data2/, monthRankingData2)
Mock.mock(/\/mock\/monthranking\/getAllsq/, monthRankingSq)

export default Mock
