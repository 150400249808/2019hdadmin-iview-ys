export default {
  errcode: 200,
  errmsg: 'ok',
  data: [
    {
      id: 30001,
      title: '老人信息管理',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '老年证申领', path: '/aged/aged-info/cardInfo', id: 3000101, icon: 'ios-keypad' },
        { title: '优待证申领', path: '/aged/aged-info/cardTreatment', id: 3000102, icon: 'ios-keypad' }
      ]
    }, {
      id: 30002,
      title: '人员迁移',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '老人迁移', path: '/aged/aged-removal/removal', id: 3000201, icon: 'ios-keypad' },
        { title: '老人迁移待审核', path: '/aged/aged-removal/out', id: 3000202, icon: 'ios-keypad' }
      ]
    }, {
      id: 30003,
      title: '老人入院管理',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '面包屑导航追加', path: '/demo/bread', id: 3000301, icon: 'ios-keypad' },
        { title: '老人入院管理', path: '/aged/aged-people-in/in', id: 3000302, icon: 'ios-keypad' }
      ]
    }, {
      id: 30004,
      title: '床位管理',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '床位管理', path: '/aged/aged-people-in/change', id: 3000401, icon: 'ios-keypad' }
      ]
    }, {
      id: 30005,
      title: '离院管理',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '离院管理', path: '/aged/aged-people-out/out', id: 3000501, icon: 'ios-keypad' }
      ]
    }, {
      id: 30006,
      title: '运营补贴',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '养老机构补贴申请', path: '/aged/subsidy/info', id: 3000601, icon: 'ios-keypad' },
        { title: '区县待审核', path: '/aged/subsidy/county-audit', id: 300602, icon: 'ios-keypad' },
        { title: '区县已审核', path: '/aged/subsidy/county-through-audit', id: 300603, icon: 'ios-keypad' },
        { title: '市级待审核', path: '/aged/subsidy/city-audit', id: 300604, icon: 'ios-keypad' },
        { title: '市级已审核', path: '/aged/subsidy/city-through-audit', id: 300605, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1001,
      title: '养老机构',
      path: '',
      icon: 'ios-appstore',
      children: [
        { title: '养老机构查询', path: '/mz/provide-aged-search', id: 100101, icon: 'ios-keypad' },
        { title: '养老机构维护', path: '/mz/provide-aged-maintain', id: 100102, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1002,
      title: '涉外婚姻',
      path: '',
      icon: 'ios-barcode-outline',
      children: [
        { title: '涉外婚姻预约', path: '/mz/marriage-subscribe', id: 100201, icon: 'ios-keypad' },
        { title: '涉外婚姻完结业务', path: '/mz/other', id: 100202, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1003,
      title: '社区工作',
      path: '',
      icon: 'md-body',
      children: [
        { title: '社区工作月度考核', path: '/mz/month-check', id: 100301, icon: 'ios-keypad' },
        { title: '社区工作季度考核', path: '/mz/quarter-check', id: 100302, icon: 'ios-keypad' },
        { title: '月度绩效排行', path: '/mz/month-ranking', id: 100303, icon: 'ios-keypad' },
        { title: '季度绩效排行', path: '/mz/other', id: 100304, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1004,
      title: '治理创新评估',
      path: '',
      icon: 'ios-bonfire',
      children: [
        { title: '评估指标模版基准设置', path: '/mz/assessment-tpl-standard-setting', id: 100401, icon: 'ios-keypad' },
        { title: '评估指标模版设置', path: '/mz/assessment-tpl-setting', id: 100402, icon: 'ios-keypad' },
        { title: '评审专家账号设置', path: '/mz/assessment-account-setting', id: 100403, icon: 'ios-keypad' },
        { title: '评估方案管理', path: '/mz/assessment-project-management', id: 100404, icon: 'ios-keypad' },
        { title: '评估申报', path: '/mz/assessment-declare', id: 100405, icon: 'ios-keypad' },
        { title: '评估审批', path: '/mz/assessment-examine-and-approve', id: 100406, icon: 'ios-keypad' },
        { title: '专家评分', path: '/mz/assessment-specialist', id: 100409, icon: 'ios-keypad' },
        { title: '评估结果审批', path: '/mz/assessment-result-examine-and-approve', id: 100407, icon: 'ios-keypad' },
        { title: '评估结果公告', path: '/mz/assessment-result-announcement', id: 100408, icon: 'ios-keypad' }

      ]
    },
    {
      id: 1005,
      title: '社区业务培训',
      path: '',
      icon: 'ios-briefcase',
      children: [
        { title: '知识库', path: '/mz/repository', id: 100501, icon: 'ios-keypad' },
        { title: '知识库维护', path: '/mz/repository-manager', id: 100505, icon: 'ios-keypad' },
        { title: '知识库类别维护', path: '/mz/other', id: 100504, icon: 'ios-keypad' },
        { title: '网格', path: '/mz/grid', id: 100502, icon: 'ios-keypad' },
        { title: '网格管理', path: '/mz/grid-manager', id: 100503, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1006,
      title: '协商议事信息',
      path: '',
      icon: 'md-bookmarks',
      children: [
        { title: '聊天', path: '/mz/layui-im', id: 100601, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1007,
      title: '殡葬',
      path: '',
      icon: 'ios-bulb',
      children: [
        { title: '殡仪叫车服务', path: '/mz/other', id: 100701, icon: 'ios-keypad' },
        { title: '殡仪安葬服务', path: '/mz/other', id: 100702, icon: 'ios-keypad' },
        { title: '基础信息查询', path: '/mz/other', id: 100703, icon: 'ios-keypad' },
        { title: '在线告别平台', path: '/mz/other', id: 100704, icon: 'ios-keypad' },
        { title: '监管评价', path: '/mz/other', id: 100705, icon: 'ios-keypad' },
        { title: '殡葬服务业务指导', path: '/mz/funeral-and-interment-business', id: 100706, icon: 'ios-keypad' },
        { title: '收费项目价格审核', path: '/mz/funeral-and-interment-price-check', id: 100707, icon: 'ios-keypad' },
        { title: '公墓审批', path: '/mz/funeral-and-interment-house-check', id: 100708, icon: 'ios-keypad' },
        { title: '公墓年检殡葬业务的审核审批', path: '/mz/funeral-and-interment-year-check', id: 100709, icon: 'ios-keypad' },
        { title: '殡葬服务单位及人员信息汇集查询', path: '/mz/funeral-and-interment-people-info', id: 100710, icon: 'ios-keypad' },
        { title: '殡葬服务单位统计', path: '/mz/funeral-and-interment-compony-statistics', id: 100714, icon: 'ios-keypad' },
        { title: '殡葬服务人员统计', path: '/mz/funeral-and-interment-people-statistics', id: 100715, icon: 'ios-keypad' },
        { title: '殡葬业务查询', path: '/mz/funeral-and-interment-search', id: 100716, icon: 'ios-keypad' },
        { title: '殡葬业务数据统计', path: '/mz/funeral-and-interment-statistics', id: 100711, icon: 'ios-keypad' },
        { title: '平台基础功能', path: '/mz/other', id: 100712, icon: 'ios-keypad' },
        { title: '海葬业务数据汇集', path: '/mz/other', id: 100713, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1008,
      title: '考核评价',
      path: '',
      icon: 'ios-medal',
      children: [
        { title: '社区数据报送考核', path: '/mz/assessment-data-submission', id: 100801, icon: 'ios-keypad' },
        { title: '工作考核', path: '/mz/service-rating', id: 100802, icon: 'ios-keypad' },
        { title: '绩效管理', path: '/mz/performance-management', id: 100803, icon: 'ios-keypad' },
        { title: '内部评价', path: '/mz/internal-evaluation', id: 100804, icon: 'ios-keypad' },
        { title: '外部评价', path: '/mz/external-evaluation', id: 100805, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1009,
      title: '社区台账',
      path: '',
      icon: 'ios-paper',
      children: [
        { title: '社区人员台账', path: '/mz/community-personnel-accounts', id: 100901, icon: 'ios-keypad' },
        { title: '网格化数据台账', path: '/mz/grid-data-account', id: 100902, icon: 'ios-keypad' },
        { title: '党组织工作台账',
          path: '',
          id: 100903,
          children: [
            { title: '人员管理', path: '/mz/personnel-management', id: 10090301, icon: 'ios-keypad' },
            { title: '工作管理', path: '/mz/work-management', id: 10090302, icon: 'ios-keypad' }
          ] },
        { title: '社会组织工作台账', path: '/mz/social-organization-workbench-account', id: 100904, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1010,
      title: 'demo菜单',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '面包屑导航追加', path: '/demo/bread', id: 101001, icon: 'ios-keypad' },
        { title: '查询页样式', path: '/demo/search-page', id: 101002, icon: 'ios-keypad' },
        { title: '详情基本样式', path: '/demo/detail', id: 101003, icon: 'ios-keypad' }
      ]
    }
  ]
}
