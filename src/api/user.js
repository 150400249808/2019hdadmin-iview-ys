import axios from '@/libs/api.request'

/*
管理端登录接口
 */
export const login = ({ userName, password }) => {
  const data = JSON.stringify({
    username: userName,
    password: password
  })
  return axios.request({
    url: '/api/uc/sc/loginService/userLogin',
    data: data,
    type: 'post'
  })
}
export const getUserInfo = () => {
  return axios.request({
    url: '/api/as/info',
    method: 'post',
    data: { a: 1 }
  })
}
export const getJavaMenus = (menu_id) => {
  return axios.request({
    url: '/api/ac/sc/menuService/getVueMenuList',
    data: { menu_id: menu_id },
    type: 'get'
  })
}
export const getPhpMenus = () => {
  return axios.request({
    url: '/wl/menus',
    type: 'get',
    php: true
  })
}
