import axios from '@/libs/api.request'

export const demo1 = (abc) => {
  return axios.request({
    url: '/Api/Api/tt',
    data: { id: abc },
    type: 'post',
    php: true
  })
}
export const demo2 = (abc) => {
  return axios.request({
    url: '/Api/Api/tt',
    data: { id: abc },
    type: 'post',
    php: true,
    await: true
  })
}

export const mock = (abc) => {
  return axios.request({
    url: '/wl/demo',
    data: { id: abc },
    type: 'post',
    php: true
  })
}

// 网格用户-树
export const grid = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmail/uslist',
    // url: '/api/as/csc/auditmail/arealist',
    method: 'get',
    data: param
  })
}
// 网格用户-楼单元层
export const auditmailfloor_getdownlist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/getdownlist',
    method: 'post',
    data: param
  })
}
// 网格用户-商户
export const auditmailcompany_uslist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailcompany/uslist',
    method: 'post',
    data: param
  })
}
// 网格用户-户信息
export const auditmailfloor_userlist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/userlist',
    method: 'post',
    await: true,
    data: param
  })
}
// 网格用户-搜索
export const auditmailfloor_getuserlist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/getuserlist',
    method: 'post',
    data: param
  })
}
// 网格用户-搜索详情
export const auditmailfloor_getuserinfo = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/getuserinfo',
    method: 'post',
    data: param
  })
}
// 网格用户-查询户数
export const auditmailfloor_gethousenum = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/gethousenum',
    method: 'post',
    await: true,
    data: param
  })
}
// 网格用户-查询户数（默认）--废弃
export const auditmailfloor_getone = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/getone',
    method: 'post',
    data: param
  })
}
// 网格用户-根据单元查户列表
export const auditmailfloor_userbyunitidlist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/userbyunitidlist',
    method: 'post',
    data: param
  })
}
// 网格管理-树
export const grid_maintain = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmail/list',
    method: 'get',
    data: param
  })
}
// 网格管理-下级列表-小组以上
export const grid_downlist = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmail/downlist',
    method: 'post',
    data: param
  })
}
// 网格管理-下级列表-小组以下
export const grid_downlist_floor = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailfloor/downlist',
    method: 'post',
    data: param
  })
}
// 网格管理-层户查询
export const auditMailFloorService_layerList = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditMailFloorService/layerList',
    method: 'post',
    data: param
  })
}
// 网格管理-创建-小组以上
export const grid_maintain_create = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmail/create',
    method: 'post',
    data: param
  })
}
// 网格管理-删除-小组以上
export const auditMailFloorService_delete = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditMailFloorService/delete',
    method: 'post',
    data: param
  })
}
// 网格管理-创建-小组以下
export const grid_maintain_create_floor = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditMailFloorService/createFloor',
    method: 'post',
    data: param
  })
}
// 网格管理-创建-小组以下(层户)
export const grid_maintain_create_floor_ceng = (param) => {
  return axios.request({
    url: '/api/ac/csc/auditMailFloorService/create',
    method: 'post',
    data: param
  })
}
// 网格管理-创建企业
export const auditmailcompany_create = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailcompany/create',
    method: 'post',
    data: param
  })
}
// 网格管理-查询企业
export const auditmailcompany_list = (param) => {
  return axios.request({
    url: '/api/as/csc/auditmailcompany/list',
    method: 'post',
    data: param
  })
}
