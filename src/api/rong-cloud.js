import axios from '@/libs/api.request'

export const loginIm = (account) => {
  return axios.request({
    url: '/Api/Api/getRongCloudUser',
    data: { account: account },
    method: 'post',
    php: true,
    await: true
  })
}
