import axios from '@/libs/api.request'

// 社区列表
export const arealist = () => {
  return axios.request({
    url: '/api/as/csc/auditmail/arealist',
    method: 'post'
  })
}
