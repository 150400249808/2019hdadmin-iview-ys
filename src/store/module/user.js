import { login, getUserInfo } from '@/api/user'
import { setToken, getToken } from '@/libs/util'

export default {
  state: {
    severUserInfo: {},
    token: getToken()
  },
  getters: {
    userInfo: state => state.severUserInfo
  },
  mutations: {
    setSeverUserInfo (state, severUserInfo) {
      state.severUserInfo = severUserInfo
    },
    setToken (state, token) {
      state.token = token
      setToken(token)
    }
  },
  actions: {
    handleLogin ({ commit }, { userName, password }) {
      userName = userName.trim()
      return new Promise((resolve, reject) => {
        login({
          userName,
          password
        }).then(res => {
          const data = res.data
          if (data.errcode === 0) {
            commit('setToken', data.data.key)
          }
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    },
    jiaLogin ({ commit }, { userName, password }) {
      commit('setToken', '3334455667788')
    },
    handleLogOut ({ state, commit }) {
      return new Promise((resolve, reject) => {
        // logout().then(() => {
        //  commit('setToken', '')
        //  commit('setAccess', [])
        //  resolve()
        // }).catch(err => {
        //  reject(err)
        // })
        // 如果你的退出登录无需请求接口，则可以直接使用下面三行代码而无需使用logout调用接口
        commit('setToken', '')
        resolve()
      })
    },
    getUserInfo ({ state, commit }) {
      return new Promise((resolve, reject) => {
        try {
          getUserInfo(state.token).then(res => {
            const data = res.data
            commit('setSeverUserInfo', data)
            resolve(data)
          }).catch(err => {
            reject(err)
          })
        } catch (error) {
          reject(error)
        }
      })
    }
  }
}
