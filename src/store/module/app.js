import { phpMenusfilter, getHomeRoute } from '@/libs/util'
import { getPhpMenus, getJavaMenus } from '@/api/user'
import localMenusData from '@/mock/data/php-menus'
import config from '@/config'

const { homeName } = config

export default {
  state: {
    breadCrumbList: [],
    menuListData: [],
    homeRoute: {}
  },
  getters: {
    getBreadCrumbList: state => state.breadCrumbList
  },
  mutations: {
    setBreadCrumb (state, data) {
      state.breadCrumbList = data
    },
    appendBreadCrumb (state, data) {
      state.breadCrumbList = data
    },
    setMenuListData (state, data) {
      state.menuListData = data
    },
    setHomeRoute (state, routes) {
      state.homeRoute = getHomeRoute(routes, homeName)
    }
  },
  actions: {
    getPhpMenusData ({ commit, rootState }, id) {
      console.log(localMenusData)
      getPhpMenus(id).then((ret) => {
        // let listData = phpMenusfilter(ret.data.data, rootState.user.access)
        // commit('setMenuListData', listData)
      })
      let listData = phpMenusfilter(localMenusData.data, rootState.user.access)
      commit('setMenuListData', listData)
    },
    getJavaMenusData ({ commit, rootState }, id) {
      getJavaMenus(id).then((ret) => {
        let listData = javaMenusfilter(ret.data.data, rootState.user.access)
        commit('setMenuListData', listData)
      })
    },
    setBreadCrumb ({ commit, rootState }, obj) {
      let menuData = obj.list
      let name = obj.name
      let is_key = 'id'
      if (name === '' || name === undefined || name === null) {
        name = obj.path
        is_key = 'path'
      }
      let BreadCrumbList = []
      menuData.forEach((item, index) => {
        if (item.children && item.children.length > 0) {
          let childData = item.children
          childData.forEach((item2, index2) => {
            if (item2.children && item2.children.length > 0) {
              let childData2 = item2.children
              childData2.forEach((item3, index3) => {
                if (item3[is_key] === name) {
                  BreadCrumbList[0] = { title: item.title, id: item.id, icon: item.icon }
                  BreadCrumbList[1] = { title: item2.title, id: item2.id, icon: item2.icon, path: item2.path }
                  BreadCrumbList[2] = { title: item3.title, id: item3.id, icon: item3.icon, path: item3.path }
                }
              })
            } else {
              if (item2[is_key] === name) {
                BreadCrumbList[0] = { title: item.title, id: item.id, icon: item.icon }
                BreadCrumbList[1] = { title: item2.title, id: item2.id, icon: item2.icon, path: item2.path }
              }
            }
          })
        }
      })
      if (menuData.length === 0) {
        commit('setBreadCrumb', [])
      } else if (BreadCrumbList.length > 0) {
        commit('setBreadCrumb', BreadCrumbList)
      }
    },
    appendBreadCrumb ({ commit, rootState }, obj) {
      let BreadCrumbList = rootState.app.breadCrumbList
      BreadCrumbList.push(obj)
      commit('appendBreadCrumb', BreadCrumbList)
    }
  }
}
