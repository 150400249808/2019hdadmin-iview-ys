import Home from '@/views/pages/home'
import demoRouter from './group/demo'
import minzhengRouter from './group/minzheng'
import aged from './group/aged'

const base = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '/index',
        name: 'index',
        component: () => import('@/views/pages/index')
      },
      {
        path: '/account',
        name: 'account',
        component: () => import('@/views/pages/weixin/account')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/pages/login')
  }
]
base[0].children.push.apply(base[0].children, demoRouter)
base[0].children.push.apply(base[0].children, minzhengRouter)
base[0].children.push.apply(base[0].children, aged)
export default base
