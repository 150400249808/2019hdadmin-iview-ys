export default [
  {
    path: '/mz/provide-aged-search',
    name: 'provide-aged-search',
    component: () => import('@/views/pages/minzheng/provide-aged-search')
  },
  {
    path: '/mz/provide-aged-maintain',
    name: 'provide-aged-maintain',
    component: () => import('@/views/pages/minzheng/provide-aged-maintain')
  },
  {
    path: '/mz/marriage-subscribe',
    name: 'marriage-subscribe',
    component: () => import('@/views/pages/minzheng/marriage-subscribe')
  },
  {
    path: '/mz/month-check',
    name: 'month-check',
    component: () => import('@/views/pages/minzheng/month-check')
  },
  {
    path: '/mz/quarter-check',
    name: 'quarter-check',
    component: () => import('@/views/pages/minzheng/quarter-check')
  },
  {
    path: '/mz/month-ranking',
    name: 'month-ranking',
    component: () => import('@/views/pages/minzheng/month-ranking')
  },
  {
    path: '/mz/assessment-tpl-standard-setting',
    name: 'assessment-tpl-standard-setting',
    component: () => import('@/views/pages/minzheng/assessment-tpl-standard-setting')
  },
  {
    path: '/mz/assessment-tpl-setting',
    name: 'assessment-tpl-setting',
    component: () => import('@/views/pages/minzheng/assessment-tpl-setting')
  },
  {
    path: '/mz/assessment-account-setting',
    name: 'assessment-account-setting',
    component: () => import('@/views/pages/minzheng/assessment-account-setting')
  },
  {
    path: '/mz/assessment-project-management',
    name: 'assessment-project-management',
    component: () => import('@/views/pages/minzheng/assessment-project-management')
  },
  {
    path: '/mz/assessment-specialist',
    name: 'assessment-specialist',
    component: () => import('@/views/pages/minzheng/assessment-specialist')
  },
  {
    path: '/mz/assessment-declare',
    name: 'assessment-declare',
    component: () => import('@/views/pages/minzheng/assessment-declare')
  },
  {
    path: '/mz/assessment-examine-and-approve',
    name: 'assessment-examine-and-approve',
    component: () => import('@/views/pages/minzheng/assessment-examine-and-approve')
  },
  {
    path: '/mz/assessment-result-examine-and-approve',
    name: 'assessment-result-examine-and-approve',
    component: () => import('@/views/pages/minzheng/assessment-result-examine-and-approve')
  },
  {
    path: '/mz/assessment-result-announcement',
    name: 'assessment-result-announcement',
    component: () => import('@/views/pages/minzheng/assessment-result-announcement')
  },
  {
    path: '/mz/assessment-list',
    name: 'assessment-result-announcement',
    component: () => import('@/views/pages/minzheng/assessment-list')
  },
  {
    path: '/mz/repository',
    name: 'repository',
    component: () => import('@/views/pages/minzheng/repository')
  },
  {
    path: '/mz/repository-manager',
    name: 'repository-manager',
    component: () => import('@/views/pages/minzheng/repository-manager')
  },
  {
    path: '/mz/grid',
    name: 'grid',
    component: () => import('@/views/pages/minzheng/grid')
  },
  {
    path: '/mz/grid-manager',
    name: 'grid-manager',
    component: () => import('@/views/pages/minzheng/grid-manager')
  },
  {
    path: '/mz/layui-im',
    name: 'layui-im',
    component: () => import('@/views/pages/minzheng/layui-im')
  },
  {
    path: '/mz/funeral-and-interment-business',
    name: 'funeral-and-interment-business',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-business')
  },
  {
    path: '/mz/funeral-and-interment-search',
    name: 'funeral-and-interment-search',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-search')
  },
  {
    path: '/mz/funeral-and-interment-price-check',
    name: 'funeral-and-interment-price-check',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-price-check')
  },
  {
    path: '/mz/funeral-and-interment-house-check',
    name: 'funeral-and-interment-house-check',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-house-check')
  },
  {
    path: '/mz/funeral-and-interment-year-check',
    name: 'funeral-and-interment-year-check',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-year-check')
  },
  {
    path: '/mz/funeral-and-interment-people-info',
    name: 'funeral-and-interment-people-info',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-people-info')
  },
  {
    path: '/mz/funeral-and-interment-statistics',
    name: 'funeral-and-interment-statistics',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-statistics')
  },
  {
    path: '/mz/funeral-and-interment-people-statistics',
    name: 'funeral-and-interment-people-statistics',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-people-statistics')
  },
  {
    path: '/mz/funeral-and-interment-compony-statistics',
    name: 'funeral-and-interment-statistics',
    component: () => import('@/views/pages/minzheng/funeral-and-interment-compony-statistics')
  },
  {
    path: '/mz/other',
    name: 'other',
    component: () => import('@/views/pages/minzheng/other')
  }
]
