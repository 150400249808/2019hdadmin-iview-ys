export default [
  {
    path: '/aged/aged-info/cardInfo',
    name: 'cardInfo',
    meta: {
      title: '老年证申领'
    },
    component: () => import('@/views/pages/aged/aged-info/card-info')
  },
  {
    path: '/aged/aged-info/cardTreatment',
    name: 'cardTreatment',
    meta: {
      title: '优待证申领'
    },
    component: () => import('@/views/pages/aged/aged-info/card-treatment')
  },
  // 人员迁移
  {
    path: '/aged/aged-removal/removal',
    name: 'peopleinfo',
    meta: {
      title: '人员迁移'
    },
    component: () => import('@/views/pages/aged/aged-removal/removal')
  },
  // 人员迁移待审核
  {
    path: '/aged/aged-removal/out',
    name: 'peopleinfo',
    meta: {
      title: '人员迁移'
    },
    component: () => import('@/views/pages/aged/aged-removal/out')
  },
  {
    path: '/aged/aged-people-out/out',
    name: 'peopleout',
    meta: {
      title: '人员迁移'
    },
    component: () => import('@/views/pages/aged/aged-people-out/out')
  },
  {
    path: '/aged/aged-removal/removal',
    name: 'removal',
    meta: {
      title: '迁移服务'
    },
    component: () => import('@/views/demo/append-bread-crumb')
  }, {
    path: '/aged/subsidy/info',
    name: 'aged/subsidy/info',
    meta: {
      title: '养老机构信息'
    },
    component: () => import('@/views/pages/aged/subsidy/info')
  },
  {
    path: '/aged/subsidy/county-audit',
    name: 'aged/subsidy/county-audit',
    meta: {
      title: '区县待审批审批'
    },
    component: () => import('@/views/pages/aged/subsidy/county-audit')
  },
  {
    path: '/aged/subsidy/county-through-audit',
    name: 'aged/subsidy/county-through-audit',
    meta: {
      title: '区县已审批'
    },
    component: () => import('@/views/pages/aged/subsidy/county-through-audit')
  },
  {
    path: '/aged/subsidy/city-through-audit',
    name: 'aged/subsidy/city-through-audit',
    meta: {
      title: '市级已审批'
    },
    component: () => import('@/views/pages/aged/subsidy/city-through-audit')
  },
  {
    path: '/aged/subsidy/city-audit',
    name: 'aged/subsidy/city-audit',
    meta: {
      title: '市级待审批'
    },
    component: () => import('@/views/pages/aged/subsidy/city-audit')
  },
  {
    path: '/aged/aged-people-in/in',
    name: 'aged/aged-people-in/in',
    meta: {
      title: '老人入院管理'
    },
    component: () => import('@/views/pages/aged/aged-people-in/in')
  },
  {
    path: '/aged/aged-people-in/change',
    name: 'aged/aged-people-in/change',
    meta: {
      title: '床位管理'
    },
    component: () => import('@/views/pages/aged/aged-people-in/change')
  }
]
