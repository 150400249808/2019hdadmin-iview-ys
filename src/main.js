import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import common from '@/libs/common'
import config from '@/config'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import '@/assets/layui/css/layui.css'
import VueAMap from 'vue-amap'

if (process.env.NODE_ENV !== 'production') require('@/mock')
Vue.use(iView)

Vue.config.productionTip = false
Vue.prototype.$common = common
Vue.prototype.$config = config
Vue.use(VueAMap)
VueAMap.initAMapApiLoader({
  key: '27c66f5b8b8179f0267de81571def171',
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor', 'Geocoder'],
  // 默认高德 sdk 版本为 1.4.4
  uiVersion: '1.0',
  v: '1.4.4'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
