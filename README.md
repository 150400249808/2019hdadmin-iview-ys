# 2019hdadmin
```
项目主要目的是搭建一套干净的pc端管理页面，增加基础的eslit语法校验，iview组件，封装axios，mock，vue-roter，vuex。实现基础的登录，菜单显示，面包屑导航，退出功能。可以在这个基础上开发新的项目系统
//TODO 下一步会把常用的功能 封装成插件用npm install 加载下来，直接维护插件功能。
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
